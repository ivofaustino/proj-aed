package AEDFINAL;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Main {
	
	
    public static void main(String[] args) throws IOException{
    	
    int[] cargo=new int [15];
    int[] valor_cargo=new int [15];
    int[] soma_de_salarios=new int [15];
    int[] soma_de_salarios_por_departamento=new int [10];
    String nao_sei="N/S";
    int soma_cargo_3=0;
    int valor_Maior=0;
    int valor_Menor=0;
    int numero_funcionarios_valor_Maior=0;
    int codigo_departamento_valor_Maior=0;
    int numero_funcionarios_valor_Menor=0;
    int codigo_departamento_valor_Menor=0;
    int ID=1;
    String ficheiro_Funcionarios = "funcionarios.txt";
    String ficheiro_Salarios = "salarios.txt";
    
    FileWriter writer = null;

    ArrayList <Funcionario>lista_funcionarios = new ArrayList<Funcionario>();
    ArrayList <Salario>lista_salario = new ArrayList<Salario>();
    
  
    
    //lista de funcionarios e preciso uma lista dinamica, falta fazer isto!!!!!
    
    Scanner scan;
    Scanner scanner;
    
    try {
		
    	scanner = new Scanner(new BufferedReader( new FileReader(ficheiro_Funcionarios)));
    	scan=scanner.useDelimiter("\\s*:\\s*|\\s*\n\\s*");
				
		while(scan.hasNextLine()){
			String nome = scan.next();
			String cartaodecidadao = scan.next();
			int departamento = scan.nextInt();
			int cargo_do_funcionario= scan.nextInt();
			
		
			Funcionario funcionario=new Funcionario(ID,nome,cartaodecidadao,departamento,cargo_do_funcionario);
			lista_funcionarios.add(funcionario);
			
			ID++;
			if (scan.hasNextLine()) scan.nextLine();
		}

		scan.close();
		scanner.close();
	ID=0;
		
	}catch (FileNotFoundException e) {
		e.printStackTrace();
		return;
	} 
    
try {
		
    	scanner = new Scanner(new BufferedReader( new FileReader(ficheiro_Salarios)));
    	scan=scanner.useDelimiter("\\s*:\\s*|\\s*\n\\s*");
				
		while(scan.hasNextLine()){
	
			int cargo_do_salario = scan.nextInt();
			int valor_do_salario= scan.nextInt();
			
			switch(cargo_do_salario){
            case 1:
                valor_cargo[0]=valor_do_salario;
                break;
            case 2:
                valor_cargo[1]=valor_do_salario;
                break;
            case 3:
                valor_cargo[2]=valor_do_salario;
                break;
            case 4:
                valor_cargo[3]=valor_do_salario;
                break;
            case 5:
                valor_cargo[4]=valor_do_salario;
                break;
            case 6:
                valor_cargo[5]=valor_do_salario;
                break;  
            case 7:
                valor_cargo[6]=valor_do_salario;
                break;
            case 8:
                valor_cargo[7]=valor_do_salario;
                break;
            case 9:
                valor_cargo[8]=valor_do_salario;
                break;
            case 10:
                valor_cargo[9]=valor_do_salario;
                break;
            case 11:
                valor_cargo[10]=valor_do_salario;
                break;
            case 12:
                valor_cargo[11]=valor_do_salario;
                break;  
            case 13:
                valor_cargo[12]=valor_do_salario;
                break;
            case 14:
                valor_cargo[13]=valor_do_salario;
                break;
            case 15:
                valor_cargo[14]=valor_do_salario;
                break;
            default:
                System.out.println("existe um erro, no ficheiro ha um cargo errado");
                
        }
		
			Salario salario=new Salario(ID, cargo_do_salario, valor_do_salario);
			lista_salario.add(salario);
			
			if (scan.hasNextLine()) scan.nextLine();
		}

		scan.close();
		scanner.close();
		
	}catch (FileNotFoundException e) {
		e.printStackTrace();
		return;
	} 
    

    //as listas foram criadas iremos percorrer a lista dos funcionarios 
    //vamos identificar o cargo e atribuir o respectivo salario, ao mesmo
    //tempo que comtamos o numero de funcionarios de 
    
    
    //_________________________________________________________________________________
    //aki dentro de cada case eu quero que faças os if()!!!!!!!!! 
    //_________________________________________________________________________________
    

    for(Funcionario func : lista_funcionarios){
    	
    
    		
        soma_de_salarios_por_departamento[(func.departamento-1)] += valor_cargo[func.cargo-1];
       
        //aki estamos a percorrer os funcionarios todos e como pretendemos apenas fazer a soma 
        //dos salarios do departamento 10, basta fazer um if() se o funcionario tiver no departamento 10 
        //ele soma o salario, e para tornar este processo muito mais rapido iremos inserir a função dendro do case
       
        //System.out.println("Nome : ( "+func.nome + "); Cartaodecidadao : (" + func.cartaodecidadao + "); Departamento : ("+ func.departamento + "); Cargo :(" + func.cargo + ")");
        
            switch(func.cargo){
                case 1:
                	
                    cargo[0]++;
                    soma_de_salarios[0]+= valor_cargo[0];
                    
                    break;
                
                case 2:
                	
                    cargo[1]++;
                    soma_de_salarios[1]+= valor_cargo[1];

                    break;
                    
                case 3:  
                    cargo[2]++;
                    soma_de_salarios[2]+= valor_cargo[2];
                  
                    soma_cargo_3+=valor_cargo[2];
                    break;
                    
                case 4:  
                    cargo[3]++;
                    soma_de_salarios[3]+= valor_cargo[3];
                    

                    break;
                case 5:  
                    cargo[4]++;
                    soma_de_salarios[4]+= valor_cargo[4];

                    break;
                case 6:  
                    cargo[5]++;
                    soma_de_salarios[5]+= valor_cargo[5];

                    break;    
                    
                case 7:  
                    cargo[6]++;
                    soma_de_salarios[6]+= valor_cargo[6];
                    
                    break;   
                case 8:  
                    cargo[7]++;
                    soma_de_salarios[7]+= valor_cargo[7];

                    break;
                case 9:  
                    cargo[8]++;
                    soma_de_salarios[8]+= valor_cargo[8];

                    break;
                case 10:  
                    cargo[9]++;
                    soma_de_salarios[9]+= valor_cargo[9];

                    break;    
                case 11:  
                    cargo[10]++;
                    soma_de_salarios[10] += valor_cargo[10];
  
                    break;
                case 12:  
                    cargo[11]++;
                    soma_de_salarios[11]+=valor_cargo[11];

                    break;
                case 13:  
                    cargo[12]++;
                    soma_de_salarios[12]+= valor_cargo[12];
                    
                    break;
                case 14:
                    cargo[13]++;
                    soma_de_salarios[13]+= valor_cargo[13];

                    break;
                case 15:  
                    cargo[14]++;
                    soma_de_salarios[14]+= valor_cargo[14];

                    break;
                    
                default:
                    System.out.println("o numero de cargo nao existe");
            }
            
    	}  
    
    
    //O metodo get_Maior() é uma modificação do alguritmo do bubllesort, a diferença entre o original e a modificaçao é que oenquanto o original
    //ordena um array de ordem crescente\descrescente, este algoritmo apena percorre o array 1 unica vez e encontra o maior\menor
    //Este algoritmo recebe um array de inteiros como parametro e trambém recebe outro que é um inteiro 0 ou 1 para identificar qual é que e o maior e queal é o menor
    
    valor_Maior=get_Maior_e_Menor(valor_cargo,1);
    valor_Menor=get_Maior_e_Menor(valor_cargo,0);
    
    codigo_departamento_valor_Maior = get_codigo_departamento_Maior_e_Menor(soma_de_salarios_por_departamento,1);
    codigo_departamento_valor_Menor = get_codigo_departamento_Maior_e_Menor(soma_de_salarios_por_departamento,0);
    
    for(int x=0;x<15;x++){
    	if(valor_cargo[x]==valor_Maior){
    		numero_funcionarios_valor_Maior=cargo[x];
    	}
    	
    	if(valor_cargo[x]==valor_Menor){
    		numero_funcionarios_valor_Menor=cargo[x];
    	}
    	
    }
    
    /*System.out.println("a soma do salarios do departamento 10 = :" + soma_departamento_10 );
    
    for(int x=0;x<15;x++){
    	System.out.println("\na soma dos salarios do cargo( " + (x+1) + " )" + " e : (" + soma_de_salarios[x] + " )");
    	System.out.println("o numero de funcionarios do cargo : ( " + (x+1) + " )" + " e : (" + cargo[x] + " )");
    }*/
    
    
    //Escrever no ficheiro as resposta!!
    
    File logFile=null;
	//BufferedWriter writer = null;
	try {
        String timeLog = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        logFile = new File(timeLog);
        //writer = new BufferedWriter(new FileWriter(logFile, true));
        logFile.createNewFile();
        
        writer = new FileWriter(logFile,true);
        
        writer.write("--1 Qual o salário da pessoa com o BI / CC = 12340050? ==> " + nao_sei + "\n");
        writer.write("--2 Qual o total de salários das pessoas do Departamento com código = 10? ==> " + soma_de_salarios_por_departamento[9] + "\n");
        writer.write("--3 Qual o total de salários das pessoas com o Cargo com código = 3? ==> " + soma_cargo_3 + "\n");
        writer.write("--4 Qual é o valor do salário mais alto que a empresa paga actualmente? ==> " + valor_Maior + "\n");
        writer.write("--5 Quantas pessoas têm o salário mais alto? ==> " + numero_funcionarios_valor_Maior + "\n");
        writer.write("--6 Qual é o valor do salário mais baixo que a empresa paga actualmente? ==> " + valor_Menor + "\n");
        writer.write("--7 Quantas pessoas têm o salário mais baixo? ==> " + numero_funcionarios_valor_Menor + "\n");
        writer.write("--8 Qual o código do departamento com o total de salários (pagos actualmente) mais alto? ==> " + codigo_departamento_valor_Maior + "\n");
        writer.write("--9 Qual o código do departamento com o total de salários (pagos actualmente) mais baixo? ==> " + codigo_departamento_valor_Menor + "\n");
        writer.write("--10 Identificar as pessoas (BI/CC) que aparecem repetidas no ficheiro_Funcionarios ==> " + nao_sei + "\n");
        
        
        writer.flush();
        writer.close();
	} catch (Exception e) {
        e.printStackTrace();
    }
    }
    
    private static int get_Maior_e_Menor(int[] array,int op) {
		
    	int[] array_copy = Arrays.copyOf(array, 15);        
        int maior=0;

        if(op==1){
        	for (int i = 0; i < array_copy.length-1; i++) {                                       
        		if (array_copy[i] > array_copy[i + 1]) {  
    	  
        			maior = array_copy[i];
        			array_copy[i] = array_copy[i + 1];
        			array_copy[i + 1] = maior;
    			
        		}
        	}

        	return array_copy[14];
        
        }else{
        	for (int i = 0; i < array_copy.length-1; i++) {                                       
        		if (array_copy[i] < array_copy[i + 1]) {  
    	  
        			maior = array_copy[i];
        			array_copy[i] = array_copy[i + 1];
        			array_copy[i + 1] = maior;
    			
        		}
        	}

        	return array_copy[14];
        }


    }
    
    //a funçao seguinte ira receber o array com a soma  do departamento e ira receber um int 0 ou 1 quera identigicar o maior\menor
    
    private static int get_codigo_departamento_Maior_e_Menor(int[] array,int op) {
		
    	int[] array_copy = Arrays.copyOf(array, 10);       
        int departamento=0;


        if(op==1){
        	for (int i = 0; i < array_copy.length-1; i++) { 
        		
        		if (array_copy[i] > array_copy[departamento]) {  
        			departamento=i;
        	
        		}
        		
        		
        	}

        	return departamento+1;
        
        }else{
        	departamento=0;
        	for (int i = 0; i < array_copy.length-1; i++) {                                       
        		if (array_copy[i] < array_copy[departamento]) {  
        			departamento=i;
    
        		}
        		
        	}

        	return departamento+1;
        }


    }
}
